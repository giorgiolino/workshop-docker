# workshop-docker > http

Image de serveur web léger basée sur [httpd:alpine](https://hub.docker.com/_/httpd/)

Commande docker de test

```bash
docker run --rm -p 80:80 $(pwd):/usr/local/apache2/htdocs httpd:alpine
```
